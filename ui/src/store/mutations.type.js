export const SET_AUTH = "setUser";
export const PURGE_AUTH = "logOut";

export const SET_ERROR = "setError";

export const TOGGLE_SIDEBAR = "toggleSidebar"

export const SET_SELECTED_ROW = "setSelectedRow";

export const TOGGLE_ADD_FORM= "toggleAddForm";
export const TOGGLE_EDIT_FORM = "toggleEditForm";

export const SET_PARTS = "setParts";
export const SET_PART = "setPart";

export const SET_NOTES = "setNotes";
export const SET_NOTE = "setNote";

export const SET_USERS = "setUsers";
export const SET_USER = "setUser";

export const SET_ROLES = "setRoles";
export const SET_ROLE = "setRole";



